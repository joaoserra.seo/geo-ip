# GeoIP Package

This package aims to provide a simply public API for dealing with the *IP2Location* derived Database used as Oxy Agency for the purposes of GeoIP Location and Network detection.

### Database

The Database expected by this package is slightly tweaked from the original, namely in the field names in order to provide more standard names and less abbreviations.

As a result this package requires that you run the *.sql files required for the IPv4 and IPv6 tables respectively, the `countries` table already has a seeder that is included with the package already, but, due to the sheer size of the IP databases, these need to be populated manually by the user.

### What does the DB Provide?

This package, when combined with the Databases mentioned above, provides you with the following.

- A list of countries, with both ISO2 and ISO3 standards for reference their calling codes *(phone number prefixes)* and currency codes.
you can be safe in the knowledge that the `countries` table included with this package contains these references for every country and not just a select few.

- You get GeoIP Information, sadly little could be done about the inconsistencies with it, you are not assured results for every IP, and some results **could** be inaccurate as the DB hasn't been updated in a while, still, it should cover most cases and passed most of my tests against both current IP2Location and Maxmind results, missing only in 1 for every 25 IPs tested, not too bad.

- You get an Operator Name for Mobile Operators which will not be available for standard Internet Providers. This is a limitation with the DB itself, however you can be relatively safe in the knowledge that you will likely get an operator name at least for any IP that is detected as being a Mobile IP Address.

### What does the Package Provide?

The Package, aims to provide easy to use wrappers around the DB calls and the parsing of the Result Set, giving you several methods that simplify access to results and operations that are commonly done against them.

You will typically use this package by instantiating the Oxy\GeoIP\GeoIpHandler class and calling the `locate` method on it, passing it any IPv4 or IPv6 address.

    $a = new Oxy\GeoIP\GeoIpHandler();
    $a->locate('86.214.11.2');
    
This will return a `LocateResult` object that contains all of the methods used to access data derived from the raw DB result set. Keep in mind that you may also omit the IP and it will try to automatically resolve it from the Server Superglobals.

    $a->locate(); // This automatically resolves the IP from the Server Superglobals.

### Locate Result Object

The `LocateResult` object, *(returned from a locate call)* will have the following public API that you can access

- hasResults() Returns a boolean indicating whether or not **any** location information is present on the DB response, you will typically get at least a `country_code` but given the inconsistencies in the DB its possible you may get nothing at all, and you're advised to use this method to make sure before you try anything else.

- isMobileConnection() Returns a boolean indicating whether or not the IP corresponds to a **Mobile** *(3G/4G)* connection as opposed to a *Wifi* connection, this is done by checking the `mobile_country_code` and `mobile_network_code` fields in the Database which **appear** to only ever appear when the network is a Mobile Connection.

- isIPv4() Returns a boolean indicating if the IP is IPv4, useful for cases where you resolve the IP automatically from the Superglobals.

- isIPv6() Same as the method above but meant for checking IPv6 instead

- countryCode() Returns the country code *(or null)* from the GeoLocation results.

- mobileOperatorName() Returns the name of the Mobile Operator if it's set from the GeoLocation results.

There, simple enough.

Besides that the `GeoIpHandler` class also provides another Public method called `getMobileOperators`, which, as the name implies will return a listing of **all** Mobile Networks and their countries from **both** the IPv4 and IPv6 tables.
The query executed to do this has been optimized to run at an average of 0.30 seconds.
However keep in mind that this depends on the indexes that are created by the existing migrations, it is possible that this performance may vary should you change them.

### OK, How do i run this?

- Run `php artisan vendor:publish` it should publish the Migration and Config files
- Run `php artisan migrate` to migrate the 3 tables required for the Package to work.
- Run `php artisan db:seed` to seed the list of Countries from the Package.
- Take the two .sql files for the IP tables and import them using an SQL tool of some sort.

Done, now you should be able to use the package as expected.
