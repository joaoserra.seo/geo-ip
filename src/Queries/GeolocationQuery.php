<?php

namespace Oxy\GeoIP\Queries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GeolocationQuery
{
    /**
     * Fetch Geolocation information for the given IP Address (converted to long string)
     *
     * @param string $ipAsLong  IP Address converted to a Long String
     * @param string $table     Name of the Table to Query (depends on IP version)
     * @param bool   $dumpQuery True if you just want to dump the SQL query, false by default
     *
     * @return Model|null
     */
    public static function get( $ipAsLong, $table, $dumpQuery = false )
    {
        if ( $dumpQuery ) {
            DB::enableQueryLog();
        }

        $result = self::runQuery($ipAsLong, $table);

        if ( $dumpQuery ) {
            dd(DB::getQueryLog());
        }

        return $result;
    }

    /**
     * How this query works
     * It will fetch all records where the block ID is less than or equal to the block end
     * but the limit by 1 makes it stop execution immediately after finding the first one,
     * so it effectively returns the first match that is less than or equal to the block_end
     *
     * This can have problems if your DB has any gaps in between blocks as it might return
     * "the next best thing" which in GeoIP can often be a whole different country.
     *
     * @param $ipAsLong
     * @param $tableName
     *
     * @return Model|null
     */
    private static function runQuery( $ipAsLong, $tableName )
    {
        $blockEndColumn = config('geoip.columns.block_end', 'block_end');

        return DB::table($tableName)->where($blockEndColumn, '>=', $ipAsLong)->limit(1)->first();
    }
}