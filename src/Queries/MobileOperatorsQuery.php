<?php

namespace Oxy\GeoIP\Queries;

use Illuminate\Support\Facades\DB;

class MobileOperatorsQuery
{
    /**
     * Returns all Mobile Operators from all IP Database Tables grouped in a Collection, uses a simple union to
     * achieve this, keep in mind that Unions are much faster than Joins and that the efficiency of this query
     * depends on the composite keys defined in the Database migration.
     *
     * @param bool $dumpQuery Indicate whether or not the Query should be dumped instead of the results
     *                        this is useful for SQL debugging.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function get( $dumpQuery = false )
    {
        if ( $dumpQuery ) {
            DB::enableQueryLog();
        }

        $ipv4Operators = DB::table(config('geoip.tables.ipv4', 'ipv4_locations'))
                           ->addSelect([
                               config('geoip.columns.country_code', 'country_code'),
                               config('geoip.columns.operator_name', 'operator_name')
                           ])
                           ->whereNotNull(config('geoip.columns.operator_name', 'operator_name'))
                           ->distinct();

        $allOperators = DB::table(config('geoip.tables.ipv6', 'ipv6_locations'))
                          ->whereNotNull(config('geoip.columns.operator_name', 'operator_name'))
                          ->distinct()
                          ->union($ipv4Operators)
                          ->get([
                              config('geoip.columns.country_code', 'country_code'),
                              config('geoip.columns.operator_name', 'operator_name')
                          ]);

        if ( $dumpQuery ) {
            dd(DB::getQueryLog());
        }

        return $allOperators;
    }
}