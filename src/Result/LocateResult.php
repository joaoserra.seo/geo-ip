<?php

namespace Oxy\GeoIP\Result;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LocateResult
 *
 * Defines the format of GeoIP Location Responses.
 *
 * @package Oxy\GeoIP\Result
 * @author  Joao Serra <joao.serra@oxy.agency>
 */
class LocateResult
{
    private $rawResults;
    private $ipVersion;
    protected $rangeStart;
    protected $rangeEnd;
    protected $mobileCountryCode;
    protected $mobileNetworkCode;
    protected $countryCode;
    protected $mobileOperatorName;

    /**
     * LocateResult constructor.
     *
     * Populates Result fields from Database Query Results.
     *
     * @param Model $dbResult
     */
    public function __construct( $dbResult, $ipVersion )
    {
        $this->ipVersion          = $ipVersion;
        $this->rawResults         = $dbResult ?? null;
        $this->rangeStart         = $dbResult->block_start ?? null;
        $this->rangeEnd           = $dbResult->block_end ?? null;
        $this->countryCode        = $dbResult->country_code ?? null;
        $this->mobileCountryCode  = $dbResult->mobile_country_code ?? null;
        $this->mobileNetworkCode  = $dbResult->mobile_network_code ?? null;
        $this->mobileOperatorName = $dbResult->operator_name ?? null;
    }

    /**
     * Returns a boolean value indicating whether or not the GeoLocation lookup
     * yielded any results.
     *
     * @return bool True if the response has any results, false otherwise.
     */
    public function hasResults()
    {
        return !!$this->rawResults && $this->checkForLocationData();
    }

    /**
     * Attempt to determine if the user is on a Mobile Connection depending on the
     * presence (or lack of presence) of MCC and MNC fields in the DB Response.
     *
     * @todo Questions as to how reliable this is.
     *       As it stands it is the only method available to us given our current DB,
     *       however, is this method really a reliable way of determining connection type?
     *
     * @return bool True if is mobile connection, False otherwise.
     */
    public function isMobileConnection()
    {
        return (!!$this->mobileNetworkCode() == true) || (!!$this->mobileCountryCode() == true);
    }

    /**
     * Check if the IP resolved is an IPV4 Format.
     *
     * @return bool True if IPv4, false otherwise
     */
    public function isIPv4()
    {
        return $this->ipVersion === 'IPv4';
    }

    /**
     * Check if the IP resolved is an IPv6
     *
     * @return bool True if IPv6, false otherwise
     */
    public function isIPv6()
    {
        return $this->ipVersion === 'IPv6';
    }

    /**
     * Get the Country Code (2 Characters) if it's available
     *
     * @return string|null
     */
    public function countryCode()
    {
        return $this->countryCode;
    }

    /**
     * Get the Mobile Network Code if it's defined.
     *
     * @return string|null
     */
    public function mobileNetworkCode()
    {
        return $this->mobileNetworkCode;
    }

    /**
     * Get the Mobile Country Code if it's defined.
     *
     * @return string|null
     */
    public function mobileCountryCode()
    {
        return $this->mobileCountryCode;
    }

    /**
     * Get the Mobile Operator Name if it's set.
     *
     * @return string|null
     */
    public function mobileOperatorName()
    {
        return $this->mobileOperatorName;
    }

    /**
     * Having checked that we have a DB Result set we need to make sure
     * that it contains at least one of our Location fields in order to
     * correctly report if it has any location results.
     *
     * @return boolean true if any one of the location fields are not null, false otherwise.
     */
    private function checkForLocationData()
    {
        $hasMobileOperator = $this->mobileOperatorName() == true;
        $countryCode       = $this->countryCode() == true;
        $mobileNetwork     = $this->mobileNetworkCode() == true;
        $mobileCountry     = $this->mobileCountryCode() == true;

        return $hasMobileOperator || $countryCode || $mobileCountry || $mobileNetwork;
    }
}