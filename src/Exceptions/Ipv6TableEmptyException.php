<?php

namespace Oxy\GeoIP\Exceptions;

use Exception;

/**
 * Class InvalidIpAddressException
 *
 * Exception Thrown by the GeoIP Handler when the IPv6 table is deemed to be void of any records.
 *
 * @package App\Hotgold\Services\GeoIP\Exceptions
 * @author  Joao Serra <joao.serra@oxy.agency>
 */
class Ipv6TableEmptyException extends Exception
{
    protected $message = "No locations detected in Ipv6 Table. Make sure it's not truncated or empty.";
}