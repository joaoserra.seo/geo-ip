<?php

namespace Oxy\GeoIP\Exceptions;

use Exception;

/**
 * Class Ipv4TableEmptyException
 *
 * Exception Thrown by the GeoIP Handler when the IPV4 Locations table is deemed to be void of any records.
 *
 * @package App\Hotgold\Services\GeoIP\Exceptions
 * @author  Joao Serra <joao.serra@oxy.agency>
 */
class Ipv4TableEmptyException extends Exception
{
    protected $message = "No locations detected in Ipv4 Table. Make sure it's not truncated or empty.";
}