<?php

namespace Oxy\GeoIP\Exceptions;

use Exception;

/**
 * Class InvalidIpAddressException
 *
 * Exception Thrown by the GeoIP Handler whenever an Invalid IP Address is entered.
 *
 * @package App\Hotgold\Services\GeoIP\Exceptions
 * @author  Joao Serra <joao.serra@oxy.agency>
 */
class InvalidIpAddressException extends Exception
{
    protected $message = "The IP Address detected is not valid.";
}