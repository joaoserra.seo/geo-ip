<?php

namespace Oxy\GeoIP\Seeders;

use Illuminate\Database\Seeder;

class GeoIPCountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->runCreateStatements();

        $this->command->info("Seeded Countries for GeoIP Package");
    }

    private function runCreateStatements()
    {
        \DB::table('countries')->insert($this->arr());
    }

    private function arr()
    {
        return [
            [
                'name'=>'Portugal',
                'iso2'=>'PT',
                'iso3'=>'PRT',
                'calling_code'=>351,
                'currency_code'=>'EUR'
            ],
            [
                'name'=>'Spain',
                'iso2'=>'ES',
                'iso3'=>'ESP',
                'calling_code'=>34,
                'currency_code'=>'EUR'
            ],
            [
                'name'=>'France',
                'iso2'=>'FR',
                'iso3'=>'FRA',
                'calling_code'=>33,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Italy',
                'iso2'=>'IT',
                'iso3'=>'ITA',
                'calling_code'=>39,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Estonia',
                'iso2'=>'EE',
                'iso3'=>'EST',
                'calling_code'=>372,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Latvia',
                'iso2'=>'LV',
                'iso3'=>'LVA',
                'calling_code'=>371,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Poland',
                'iso2'=>'PL',
                'iso3'=>'POL',
                'calling_code'=>48,
                'currency_code'=>'PLN'
            ],        [
                'name'=>'Russia',
                'iso2'=>'RU',
                'iso3'=>'RUS',
                'calling_code'=>7,
                'currency_code'=>'RUB'
            ],        [
                'name'=>'Canada',
                'iso2'=>'CA',
                'iso3'=>'CAN',
                'calling_code'=>1,
                'currency_code'=>'CAD'
            ],        [
                'name'=>'Malasya',
                'iso2'=>'MY',
                'iso3'=>'MYS',
                'calling_code'=>60,
                'currency_code'=>'MYR'
            ],        [
                'name'=>'Slovenia',
                'iso2'=>'SI',
                'iso3'=>'SVN',
                'calling_code'=>386,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Brazil',
                'iso2'=>'BR',
                'iso3'=>'BRA',
                'calling_code'=>55,
                'currency_code'=>'BRL'
            ],        [
                'name'=>'Hungary',
                'iso2'=>'HU',
                'iso3'=>'HUN',
                'calling_code'=>36,
                'currency_code'=>'HUF'
            ],        [
                'name'=>'Netherlands',
                'iso2'=>'NL',
                'iso3'=>'NLD',
                'calling_code'=>31,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Greece',
                'iso2'=>'GR',
                'iso3'=>'GRC',
                'calling_code'=>30,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Angola',
                'iso2'=>'AO',
                'iso3'=>'AGO',
                'calling_code'=>244,
                'currency_code'=>'AOA'
            ],        [
                'name'=>'Mexico',
                'iso2'=>'MX',
                'iso3'=>'MEX',
                'calling_code'=>52,
                'currency_code'=>'MXN'
            ],        [
                'name'=>'South Africa',
                'iso2'=>'ZA',
                'iso3'=>'ZAF',
                'calling_code'=>27,
                'currency_code'=>'ZAR'
            ],        [
                'name'=>'United Kingdom',
                'iso2'=>'GB',
                'iso3'=>'GBR',
                'calling_code'=>44,
                'currency_code'=>'GBP'
            ],        [
                'name'=>'United Arab Emirates',
                'iso2'=>'AE',
                'iso3'=>'ARE',
                'calling_code'=>971,
                'currency_code'=>'AED'
            ],        [
                'name'=>'Chile',
                'iso2'=>'CL',
                'iso3'=>'CHL',
                'calling_code'=>56,
                'currency_code'=>'CLP'
            ],        [
                'name'=>'Belgium',
                'iso2'=>'BE',
                'iso3'=>'BEL',
                'calling_code'=>32,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Colombia',
                'iso2'=>'CO',
                'iso3'=>'COL',
                'calling_code'=>57,
                'currency_code'=>'COP'
            ],        [
                'name'=>'Argentina',
                'iso2'=>'AR',
                'iso3'=>'ARG',
                'calling_code'=>54,
                'currency_code'=>'ARS'
            ],        [
                'name'=>'Israel',
                'iso2'=>'IL',
                'iso3'=>'ISR',
                'calling_code'=>972,
                'currency_code'=>'ILS'
            ],        [
                'name'=>'United States of America',
                'iso2'=>'US',
                'iso3'=>'USA',
                'calling_code'=>1,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Kenya',
                'iso2'=>'KE',
                'iso3'=>'KEN',
                'calling_code'=>254,
                'currency_code'=>'KES'
            ],        [
                'name'=>'Ecuador',
                'iso2'=>'EC',
                'iso3'=>'ECU',
                'calling_code'=>593,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Guatemala',
                'iso2'=>'GT',
                'iso3'=>'GTM',
                'calling_code'=>502,
                'currency_code'=>'GTQ'
            ],        [
                'name'=>'Peru',
                'iso2'=>'PE',
                'iso3'=>'PER',
                'calling_code'=>51,
                'currency_code'=>'PEN'
            ],        [
                'name'=>'Panama',
                'iso2'=>'PA',
                'iso3'=>'PAN',
                'calling_code'=>507,
                'currency_code'=>'PAB'
            ],        [
                'name'=>'Nicaragua',
                'iso2'=>'NI',
                'iso3'=>'NIC',
                'calling_code'=>505,
                'currency_code'=>'NIO'
            ],        [
                'name'=>'Australia',
                'iso2'=>'AU',
                'iso3'=>'AUS',
                'calling_code'=>61,
                'currency_code'=>'AUD'
            ],        [
                'name'=>'South Korea',
                'iso2'=>'KR',
                'iso3'=>'KOR',
                'calling_code'=>82,
                'currency_code'=>'KRW'
            ],        [
                'name'=>'Singapore',
                'iso2'=>'SG',
                'iso3'=>'SGP',
                'calling_code'=>65,
                'currency_code'=>'SGD'
            ],        [
                'name'=>'Hong Kong',
                'iso2'=>'HK',
                'iso3'=>'HKG',
                'calling_code'=>852,
                'currency_code'=>'HKD'
            ],        [
                'name'=>'Sweden',
                'iso2'=>'SE',
                'iso3'=>'SWE',
                'calling_code'=>46,
                'currency_code'=>'SEK'
            ],        [
                'name'=>'Finland',
                'iso2'=>'FI',
                'iso3'=>'FIN',
                'calling_code'=>358,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Denmark',
                'iso2'=>'DK',
                'iso3'=>'DNK',
                'calling_code'=>45,
                'currency_code'=>'DKK'
            ],        [
                'name'=>'Norway',
                'iso2'=>'NO',
                'iso3'=>'NOR',
                'calling_code'=>47,
                'currency_code'=>'NOK'
            ],        [
                'name'=>'Romania',
                'iso2'=>'RO',
                'iso3'=>'ROU',
                'calling_code'=>40,
                'currency_code'=>'RON'
            ],        [
                'name'=>'Dominican Republic',
                'iso2'=>'DO',
                'iso3'=>'DOM',
                'calling_code'=>1809,
                'currency_code'=>'DOP'
            ],        [
                'name'=>'El Salvador',
                'iso2'=>'SV',
                'iso3'=>'SLV',
                'calling_code'=>503,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Iraq',
                'iso2'=>'IQ',
                'iso3'=>'IRQ',
                'calling_code'=>964,
                'currency_code'=>'IQD'
            ],        [
                'name'=>'Honduras',
                'iso2'=>'HN',
                'iso3'=>'HND',
                'calling_code'=>504,
                'currency_code'=>'HNL'
            ],        [
                'name'=>'Qatar',
                'iso2'=>'QA',
                'iso3'=>'QAT',
                'calling_code'=>974,
                'currency_code'=>'QAR'
            ],        [
                'name'=>'Saudi Arabia',
                'iso2'=>'SA',
                'iso3'=>'SAU',
                'calling_code'=>966,
                'currency_code'=>'SAR'
            ],        [
                'name'=>'Venezuela',
                'iso2'=>'VE',
                'iso3'=>'VEN',
                'calling_code'=>58,
                'currency_code'=>'VEF'
            ],        [
                'name'=>'Uruguay',
                'iso2'=>'UY',
                'iso3'=>'URY',
                'calling_code'=>598,
                'currency_code'=>'UYU'
            ],        [
                'name'=>'Bolivia',
                'iso2'=>'BO',
                'iso3'=>'BOL',
                'calling_code'=>591,
                'currency_code'=>'BOB'
            ],        [
                'name'=>'Bahrain',
                'iso2'=>'BH',
                'iso3'=>'BHR',
                'calling_code'=>973,
                'currency_code'=>'BHD'
            ],        [
                'name'=>'Jordan',
                'iso2'=>'JO',
                'iso3'=>'JOR',
                'calling_code'=>962,
                'currency_code'=>'JOD'
            ],        [
                'name'=>'Ukraine',
                'iso2'=>'UA',
                'iso3'=>'UKR',
                'calling_code'=>380,
                'currency_code'=>'UAH'
            ],        [
                'name'=>'Indonesia',
                'iso2'=>'ID',
                'iso3'=>'IDN',
                'calling_code'=>62,
                'currency_code'=>'IDR'
            ],        [
                'name'=>'New Zeland',
                'iso2'=>'NZ',
                'iso3'=>'NZL',
                'calling_code'=>64,
                'currency_code'=>'NZD'
            ],        [
                'name'=>'Morocco ',
                'iso2'=>'MA',
                'iso3'=>'MAR',
                'calling_code'=>212,
                'currency_code'=>'MAD'
            ],        [
                'name'=>'Serbia',
                'iso2'=>'RS',
                'iso3'=>'SRB',
                'calling_code'=>381,
                'currency_code'=>'RSD'
            ],        [
                'name'=>'Vietnam',
                'iso2'=>'VN',
                'iso3'=>'VNM',
                'calling_code'=>84,
                'currency_code'=>'VND'
            ],        [
                'name'=>'India',
                'iso2'=>'IN',
                'iso3'=>'IND',
                'calling_code'=>91,
                'currency_code'=>'INR'
            ],        [
                'name'=>'Ghana',
                'iso2'=>'GH',
                'iso3'=>'GHA',
                'calling_code'=>233,
                'currency_code'=>'GHS'
            ],        [
                'name'=>'Kuwait',
                'iso2'=>'KW',
                'iso3'=>'KWT',
                'calling_code'=>965,
                'currency_code'=>'KWD'
            ],        [
                'name'=>'Cape Verde',
                'iso2'=>'CV',
                'iso3'=>'CPV',
                'calling_code'=>238,
                'currency_code'=>'CVE'
            ],        [
                'name'=>'China',
                'iso2'=>'CN',
                'iso3'=>'CHN',
                'calling_code'=>86,
                'currency_code'=>'CNY'
            ],        [
                'name'=>'Thailand',
                'iso2'=>'TH',
                'iso3'=>'THA',
                'calling_code'=>66,
                'currency_code'=>'THB'
            ],        [
                'name'=>'Bangladesh',
                'iso2'=>'BD',
                'iso3'=>'BGD',
                'calling_code'=>880,
                'currency_code'=>'BDT'
            ],        [
                'name'=>'Pakistan',
                'iso2'=>'PK',
                'iso3'=>'PAK',
                'calling_code'=>92,
                'currency_code'=>'PKR'
            ],        [
                'name'=>'Austria',
                'iso2'=>'AT',
                'iso3'=>'AUT',
                'calling_code'=>43,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Germany',
                'iso2'=>'DE',
                'iso3'=>'DEU',
                'calling_code'=>49,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Switzerland',
                'iso2'=>'CH',
                'iso3'=>'CHE',
                'calling_code'=>41,
                'currency_code'=>'CHF'
            ],        [
                'name'=>'Egypt',
                'iso2'=>'EG',
                'iso3'=>'EGY',
                'calling_code'=>20,
                'currency_code'=>'EGP'
            ],        [
                'name'=>'Turkey',
                'iso2'=>'TR',
                'iso3'=>'TUR',
                'calling_code'=>90,
                'currency_code'=>'TRY'
            ],        [
                'name'=>'Taiwan',
                'iso2'=>'TW',
                'iso3'=>'TWN',
                'calling_code'=>886,
                'currency_code'=>'TWD'
            ],        [
                'name'=>'Costa Rica',
                'iso2'=>'CR',
                'iso3'=>'CRI',
                'calling_code'=>506,
                'currency_code'=>'CRC'
            ],        [
                'name'=>'Nigeria',
                'iso2'=>'NG',
                'iso3'=>'NGA',
                'calling_code'=>234,
                'currency_code'=>'NGN'
            ],        [
                'name'=>'Philippines',
                'iso2'=>'PH',
                'iso3'=>'PHL',
                'calling_code'=>63,
                'currency_code'=>'PHP'
            ],        [
                'name'=>'Slovakia',
                'iso2'=>'SK',
                'iso3'=>'SVK',
                'calling_code'=>421,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Czech Republic',
                'iso2'=>'CZ',
                'iso3'=>'CZE',
                'calling_code'=>420,
                'currency_code'=>'CZK'
            ],        [
                'name'=>'Japan',
                'iso2'=>'JP',
                'iso3'=>'JPN',
                'calling_code'=>81,
                'currency_code'=>'JPY'
            ],        [
                'name'=>'Uganda',
                'iso2'=>'UG',
                'iso3'=>'UGA',
                'calling_code'=>256,
                'currency_code'=>'UGX'
            ],        [
                'name'=>'Ireland',
                'iso2'=>'IE',
                'iso3'=>'IRL',
                'calling_code'=>353,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Palestine',
                'iso2'=>'PS',
                'iso3'=>'PSE',
                'calling_code'=>970,
                'currency_code'=>'ILS'
            ],        [
                'name'=>'Lebanon',
                'iso2'=>'LB',
                'iso3'=>'LBN',
                'calling_code'=>961,
                'currency_code'=>'LBP'
            ],        [
                'name'=>'Croatia',
                'iso2'=>'HR',
                'iso3'=>'HRV',
                'calling_code'=>385,
                'currency_code'=>'HRK'
            ],        [
                'name'=>'Cyprus',
                'iso2'=>'CY',
                'iso3'=>'CYP',
                'calling_code'=>357,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Paraguay',
                'iso2'=>'PY',
                'iso3'=>'PRY',
                'calling_code'=>595,
                'currency_code'=>'PYG'
            ],        [
                'name'=>'Bulgaria',
                'iso2'=>'BG',
                'iso3'=>'BGR',
                'calling_code'=>359,
                'currency_code'=>'BGN'
            ],        [
                'name'=>'Bosnia and Herzegovina',
                'iso2'=>'BA',
                'iso3'=>'BIH',
                'calling_code'=>387,
                'currency_code'=>'BAM'
            ],        [
                'name'=>'Montenegro',
                'iso2'=>'ME',
                'iso3'=>'MNE',
                'calling_code'=>382,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Macedonia',
                'iso2'=>'MK',
                'iso3'=>'MKD',
                'calling_code'=>389,
                'currency_code'=>'MKD'
            ],        [
                'name'=>'Azerbaijan',
                'iso2'=>'AZ',
                'iso3'=>'AZE',
                'calling_code'=>994,
                'currency_code'=>'AZN'
            ],        [
                'name'=>'Sri Lanka',
                'iso2'=>'LK',
                'iso3'=>'LKA',
                'calling_code'=>94,
                'currency_code'=>'LKR'
            ],        [
                'name'=>'Luxembourg',
                'iso2'=>'LU',
                'iso3'=>'LUX',
                'calling_code'=>352,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Kazakhstan',
                'iso2'=>'KZ',
                'iso3'=>'KAZ',
                'calling_code'=>7,
                'currency_code'=>'KZT'
            ],        [
                'name'=>'Afghanistan',
                'iso2'=>'AF',
                'iso3'=>'AFG',
                'calling_code'=>93,
                'currency_code'=>'AFN'
            ],        [
                'name'=>'Albania',
                'iso2'=>'AL',
                'iso3'=>'ALB',
                'calling_code'=>355,
                'currency_code'=>'ALL'
            ],        [
                'name'=>'Algeria',
                'iso2'=>'DZ',
                'iso3'=>'DZA',
                'calling_code'=>213,
                'currency_code'=>'DZD'
            ],        [
                'name'=>'American Samoa',
                'iso2'=>'AS',
                'iso3'=>'ASM',
                'calling_code'=>1684,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Andorra',
                'iso2'=>'AD',
                'iso3'=>'AND',
                'calling_code'=>376,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Anguilla',
                'iso2'=>'AI',
                'iso3'=>'AIA',
                'calling_code'=>1264,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Antigua and Barbuda',
                'iso2'=>'AG',
                'iso3'=>'ATG',
                'calling_code'=>1268,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Armenia',
                'iso2'=>'AM',
                'iso3'=>'ARM',
                'calling_code'=>374,
                'currency_code'=>'AMD'
            ],        [
                'name'=>'Aruba',
                'iso2'=>'AW',
                'iso3'=>'ABW',
                'calling_code'=>297,
                'currency_code'=>'AWG'
            ],        [
                'name'=>'Bahamas',
                'iso2'=>'BS',
                'iso3'=>'BHS',
                'calling_code'=>1242,
                'currency_code'=>'BSD'
            ],        [
                'name'=>'Barbados',
                'iso2'=>'BB',
                'iso3'=>'BRB',
                'calling_code'=>1246,
                'currency_code'=>'BBD'
            ],        [
                'name'=>'Belarus',
                'iso2'=>'BY',
                'iso3'=>'BLR',
                'calling_code'=>375,
                'currency_code'=>'BYR'
            ],        [
                'name'=>'Belize',
                'iso2'=>'BZ',
                'iso3'=>'BLZ',
                'calling_code'=>501,
                'currency_code'=>'BZD'
            ],        [
                'name'=>'Benin',
                'iso2'=>'BJ',
                'iso3'=>'BEN',
                'calling_code'=>229,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Bermuda',
                'iso2'=>'BM',
                'iso3'=>'BMU',
                'calling_code'=>1441,
                'currency_code'=>'BMD'
            ],        [
                'name'=>'Bhutan',
                'iso2'=>'BT',
                'iso3'=>'BTN',
                'calling_code'=>975,
                'currency_code'=>'BTN'
            ],        [
                'name'=>'Botswana',
                'iso2'=>'BW',
                'iso3'=>'BWA',
                'calling_code'=>267,
                'currency_code'=>'BWP'
            ],        [
                'name'=>'British Virgin Islands',
                'iso2'=>'VG',
                'iso3'=>'VGB',
                'calling_code'=>1284,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Brunei',
                'iso2'=>'BN',
                'iso3'=>'BRN',
                'calling_code'=>673,
                'currency_code'=>'BND'
            ],        [
                'name'=>'Burkina Faso',
                'iso2'=>'BF',
                'iso3'=>'BFA',
                'calling_code'=>226,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Burundi',
                'iso2'=>'BI',
                'iso3'=>'BDI',
                'calling_code'=>257,
                'currency_code'=>'BIF'
            ],        [
                'name'=>'Cambodia',
                'iso2'=>'KH',
                'iso3'=>'KHM',
                'calling_code'=>855,
                'currency_code'=>'KHR'
            ],        [
                'name'=>'Cameroon',
                'iso2'=>'CM',
                'iso3'=>'CMR',
                'calling_code'=>237,
                'currency_code'=>'XAF'
            ],        [
                'name'=>'Cayman Islands',
                'iso2'=>'KY',
                'iso3'=>'CYM',
                'calling_code'=>1345,
                'currency_code'=>'KYD'
            ],        [
                'name'=>'Central African Republic',
                'iso2'=>'CF',
                'iso3'=>'CAF',
                'calling_code'=>236,
                'currency_code'=>'XAF'
            ],        [
                'name'=>'Chad',
                'iso2'=>'TD',
                'iso3'=>'TCD',
                'calling_code'=>235,
                'currency_code'=>'XAF'
            ],        [
                'name'=>'Comoros',
                'iso2'=>'KM',
                'iso3'=>'COM',
                'calling_code'=>269,
                'currency_code'=>'KMF'
            ],        [
                'name'=>'Democratic Republic of the Congo',
                'iso2'=>'CD',
                'iso3'=>'COD',
                'calling_code'=>243,
                'currency_code'=>'CDF'
            ],        [
                'name'=>'Republic of the Congo',
                'iso2'=>'CG',
                'iso3'=>'COG',
                'calling_code'=>242,
                'currency_code'=>'XAF'
            ],        [
                'name'=>'Cook Islands',
                'iso2'=>'CK',
                'iso3'=>'COK',
                'calling_code'=>682,
                'currency_code'=>'NZD'
            ],        [
                'name'=>'Cuba',
                'iso2'=>'CU',
                'iso3'=>'CUB',
                'calling_code'=>53,
                'currency_code'=>'CUP'
            ],        [
                'name'=>'Curacao',
                'iso2'=>'CW',
                'iso3'=>'CUW',
                'calling_code'=>599,
                'currency_code'=>'ANG'
            ],        [
                'name'=>'Djibouti',
                'iso2'=>'DJ',
                'iso3'=>'DJI',
                'calling_code'=>253,
                'currency_code'=>'DJF'
            ],        [
                'name'=>'Dominica',
                'iso2'=>'DM',
                'iso3'=>'DMA',
                'calling_code'=>1767,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Equatorial Guinea',
                'iso2'=>'GQ',
                'iso3'=>'GNQ',
                'calling_code'=>240,
                'currency_code'=>'XAF'
            ],        [
                'name'=>'Eritrea',
                'iso2'=>'ER',
                'iso3'=>'ERI',
                'calling_code'=>291,
                'currency_code'=>'ERN'
            ],        [
                'name'=>'Ethiopia',
                'iso2'=>'ET',
                'iso3'=>'ETH',
                'calling_code'=>251,
                'currency_code'=>'ETB'
            ],        [
                'name'=>'Falkland Islands',
                'iso2'=>'FK',
                'iso3'=>'FLK',
                'calling_code'=>500,
                'currency_code'=>'FKP'
            ],        [
                'name'=>'Faroe Islands',
                'iso2'=>'FO',
                'iso3'=>'FRO',
                'calling_code'=>298,
                'currency_code'=>'DKK'
            ],        [
                'name'=>'Fiji',
                'iso2'=>'FJ',
                'iso3'=>'FJI',
                'calling_code'=>679,
                'currency_code'=>'FJD'
            ],        [
                'name'=>'French Guiana',
                'iso2'=>'FG',
                'iso3'=>'GUF',
                'calling_code'=>594,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'French Polynesia',
                'iso2'=>'PF',
                'iso3'=>'PYF',
                'calling_code'=>689,
                'currency_code'=>'XPF'
            ],        [
                'name'=>'Gabon',
                'iso2'=>'GA',
                'iso3'=>'GAB',
                'calling_code'=>241,
                'currency_code'=>'XAF'
            ],        [
                'name'=>'Gambia',
                'iso2'=>'GM',
                'iso3'=>'GMB',
                'calling_code'=>220,
                'currency_code'=>'GMD'
            ],        [
                'name'=>'Georgia',
                'iso2'=>'GE',
                'iso3'=>'GEO',
                'calling_code'=>995,
                'currency_code'=>'GEL'
            ],        [
                'name'=>'Gibraltar',
                'iso2'=>'GI',
                'iso3'=>'GIB',
                'calling_code'=>350,
                'currency_code'=>'GIP'
            ],        [
                'name'=>'Greenland',
                'iso2'=>'GL',
                'iso3'=>'GRL',
                'calling_code'=>299,
                'currency_code'=>'DKK'
            ],        [
                'name'=>'Grenada',
                'iso2'=>'GD',
                'iso3'=>'GRD',
                'calling_code'=>1473,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Guadeloupe ',
                'iso2'=>'GP',
                'iso3'=>'GLP',
                'calling_code'=>590,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Guam',
                'iso2'=>'GU',
                'iso3'=>'GUM',
                'calling_code'=>1671,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Guinea',
                'iso2'=>'GN',
                'iso3'=>'GIN',
                'calling_code'=>224,
                'currency_code'=>'GNF'
            ],        [
                'name'=>'Guinea-Bissau',
                'iso2'=>'GW',
                'iso3'=>'GNB',
                'calling_code'=>245,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Guyana',
                'iso2'=>'GY',
                'iso3'=>'GUY',
                'calling_code'=>592,
                'currency_code'=>'GYD'
            ],        [
                'name'=>'Haiti',
                'iso2'=>'HT',
                'iso3'=>'HTI',
                'calling_code'=>509,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Iceland',
                'iso2'=>'IS',
                'iso3'=>'ISL',
                'calling_code'=>354,
                'currency_code'=>'ISK'
            ],        [
                'name'=>'Iran',
                'iso2'=>'IR',
                'iso3'=>'IRN',
                'calling_code'=>98,
                'currency_code'=>'IRR'
            ],        [
                'name'=>'Ivory Coast',
                'iso2'=>'CI',
                'iso3'=>'CIV',
                'calling_code'=>225,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Jamaica',
                'iso2'=>'JM',
                'iso3'=>'JAM',
                'calling_code'=>1876,
                'currency_code'=>'JMD'
            ],        [
                'name'=>'Kiribati',
                'iso2'=>'KI',
                'iso3'=>'KIR',
                'calling_code'=>686,
                'currency_code'=>'AUD'
            ],        [
                'name'=>'North Korea',
                'iso2'=>'KP',
                'iso3'=>'PRK',
                'calling_code'=>850,
                'currency_code'=>'KPW'
            ],        [
                'name'=>'Kyrgyzstan',
                'iso2'=>'KG',
                'iso3'=>'KGZ',
                'calling_code'=>996,
                'currency_code'=>'KGS'
            ],        [
                'name'=>'Laos',
                'iso2'=>'LA',
                'iso3'=>'LAO',
                'calling_code'=>856,
                'currency_code'=>'LAK'
            ],        [
                'name'=>'Lesotho',
                'iso2'=>'LS',
                'iso3'=>'LSO',
                'calling_code'=>266,
                'currency_code'=>'LSL'
            ],        [
                'name'=>'Liberia',
                'iso2'=>'LR',
                'iso3'=>'LBR',
                'calling_code'=>231,
                'currency_code'=>'LRD'
            ],        [
                'name'=>'Libya',
                'iso2'=>'LY',
                'iso3'=>'LBY',
                'calling_code'=>218,
                'currency_code'=>'LYD'
            ],        [
                'name'=>'Liechtenstein',
                'iso2'=>'LI',
                'iso3'=>'LIE',
                'calling_code'=>423,
                'currency_code'=>'CHF'
            ],        [
                'name'=>'Lithuania',
                'iso2'=>'LT',
                'iso3'=>'LTU',
                'calling_code'=>370,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Macau',
                'iso2'=>'MO',
                'iso3'=>'MAC',
                'calling_code'=>853,
                'currency_code'=>'MOP'
            ],        [
                'name'=>'Madagascar',
                'iso2'=>'MG',
                'iso3'=>'MDG',
                'calling_code'=>261,
                'currency_code'=>'MGA'
            ],        [
                'name'=>'Malawi',
                'iso2'=>'MW',
                'iso3'=>'MWI',
                'calling_code'=>265,
                'currency_code'=>'MWK'
            ],        [
                'name'=>'Maldives',
                'iso2'=>'MV',
                'iso3'=>'MDV',
                'calling_code'=>960,
                'currency_code'=>'MVR'
            ],        [
                'name'=>'Mali',
                'iso2'=>'ML',
                'iso3'=>'MLI',
                'calling_code'=>223,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Malta',
                'iso2'=>'MT',
                'iso3'=>'MLT',
                'calling_code'=>356,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Martinique',
                'iso2'=>'MQ',
                'iso3'=>'MTQ',
                'calling_code'=>596,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Mauritania',
                'iso2'=>'MR',
                'iso3'=>'MRT',
                'calling_code'=>222,
                'currency_code'=>'MRO'
            ],        [
                'name'=>'Mauritius',
                'iso2'=>'MU',
                'iso3'=>'MUS',
                'calling_code'=>230,
                'currency_code'=>'MUR'
            ],        [
                'name'=>'Micronesia',
                'iso2'=>'FM',
                'iso3'=>'FSM',
                'calling_code'=>691,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Moldova',
                'iso2'=>'MD',
                'iso3'=>'MDA',
                'calling_code'=>373,
                'currency_code'=>'MDL'
            ],        [
                'name'=>'Monaco',
                'iso2'=>'MC',
                'iso3'=>'MCO',
                'calling_code'=>377,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Mongolia',
                'iso2'=>'MN',
                'iso3'=>'MNG',
                'calling_code'=>976,
                'currency_code'=>'MNT'
            ],        [
                'name'=>'Montserrat',
                'iso2'=>'MS',
                'iso3'=>'MSR',
                'calling_code'=>1664,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Mozambique',
                'iso2'=>'MZ',
                'iso3'=>'MOZ',
                'calling_code'=>258,
                'currency_code'=>'MZN'
            ],        [
                'name'=>'Myanmar',
                'iso2'=>'MM',
                'iso3'=>'MMR',
                'calling_code'=>95,
                'currency_code'=>'MMK'
            ],        [
                'name'=>'Namibia',
                'iso2'=>'NA',
                'iso3'=>'NAM',
                'calling_code'=>264,
                'currency_code'=>'ZAR'
            ],        [
                'name'=>'Nepal',
                'iso2'=>'NP',
                'iso3'=>'NPL',
                'calling_code'=>977,
                'currency_code'=>'NPR'
            ],        [
                'name'=>'Netherlands Antilles',
                'iso2'=>'AN',
                'iso3'=>'ANT',
                'calling_code'=>599,
                'currency_code'=>'ANG'
            ],        [
                'name'=>'New Caledonia',
                'iso2'=>'NC',
                'iso3'=>'NCL',
                'calling_code'=>687,
                'currency_code'=>'XPF'
            ],        [
                'name'=>'Niger',
                'iso2'=>'NE',
                'iso3'=>'NER',
                'calling_code'=>227,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Niue',
                'iso2'=>'NU',
                'iso3'=>'NIU',
                'calling_code'=>683,
                'currency_code'=>'NZD'
            ],        [
                'name'=>'Oman',
                'iso2'=>'OM',
                'iso3'=>'OMN',
                'calling_code'=>968,
                'currency_code'=>'OMR'
            ],        [
                'name'=>'Palau',
                'iso2'=>'PW',
                'iso3'=>'PLW',
                'calling_code'=>680,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Papua New Guinea',
                'iso2'=>'PG',
                'iso3'=>'PNG',
                'calling_code'=>675,
                'currency_code'=>'PGK'
            ],        [
                'name'=>'Puerto Rico',
                'iso2'=>'PR',
                'iso3'=>'PRI',
                'calling_code'=>1,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Reunion',
                'iso2'=>'RE',
                'iso3'=>'REU',
                'calling_code'=>262,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Rwanda',
                'iso2'=>'RW',
                'iso3'=>'RWA',
                'calling_code'=>250,
                'currency_code'=>'RWF'
            ],        [
                'name'=>'Saint Kitts and Nevis',
                'iso2'=>'KN',
                'iso3'=>'KNA',
                'calling_code'=>1869,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Saint Lucia',
                'iso2'=>'LC',
                'iso3'=>'LCA',
                'calling_code'=>1758,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Samoa',
                'iso2'=>'WS',
                'iso3'=>'WSM',
                'calling_code'=>685,
                'currency_code'=>'WST'
            ],        [
                'name'=>'San Marino',
                'iso2'=>'SM',
                'iso3'=>'SMR',
                'calling_code'=>378,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Sao Tome & Principe',
                'iso2'=>'ST',
                'iso3'=>'STP',
                'calling_code'=>239,
                'currency_code'=>'STD'
            ],        [
                'name'=>'Senegal',
                'iso2'=>'SN',
                'iso3'=>'SEN',
                'calling_code'=>221,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Seychelles',
                'iso2'=>'SC',
                'iso3'=>'SYC',
                'calling_code'=>248,
                'currency_code'=>'SCR'
            ],        [
                'name'=>'Sierra Leone',
                'iso2'=>'SL',
                'iso3'=>'SLE',
                'calling_code'=>232,
                'currency_code'=>'SLL'
            ],        [
                'name'=>'Solomon Islands',
                'iso2'=>'SB',
                'iso3'=>'SLB',
                'calling_code'=>677,
                'currency_code'=>'SBD'
            ],        [
                'name'=>'Somalia',
                'iso2'=>'SO',
                'iso3'=>'SOM',
                'calling_code'=>252,
                'currency_code'=>'SOS'
            ],        [
                'name'=>'South Sudan',
                'iso2'=>'SS',
                'iso3'=>'SSD',
                'calling_code'=>211,
                'currency_code'=>'SSP'
            ],        [
                'name'=>'Saint Pierre and Miquelon',
                'iso2'=>'PM',
                'iso3'=>'SPM',
                'calling_code'=>508,
                'currency_code'=>'EUR'
            ],        [
                'name'=>'Saint Vincent and the Grenadines',
                'iso2'=>'VC',
                'iso3'=>'VCT',
                'calling_code'=>1784,
                'currency_code'=>'XCD'
            ],        [
                'name'=>'Sudan',
                'iso2'=>'SD',
                'iso3'=>'SDN',
                'calling_code'=>249,
                'currency_code'=>'SDG'
            ],        [
                'name'=>'Suriname',
                'iso2'=>'SR',
                'iso3'=>'SUR',
                'calling_code'=>597,
                'currency_code'=>'SRD'
            ],        [
                'name'=>'Swaziland',
                'iso2'=>'SZ',
                'iso3'=>'SWZ',
                'calling_code'=>268,
                'currency_code'=>'SZL'
            ],        [
                'name'=>'Syria',
                'iso2'=>'SY',
                'iso3'=>'SYR',
                'calling_code'=>963,
                'currency_code'=>'SYP'
            ],        [
                'name'=>'Tajikistan',
                'iso2'=>'TK',
                'iso3'=>'TJK',
                'calling_code'=>992,
                'currency_code'=>'TJS'
            ],        [
                'name'=>'Tanzania',
                'iso2'=>'TZ',
                'iso3'=>'TZA',
                'calling_code'=>255,
                'currency_code'=>'TZS'
            ],        [
                'name'=>'East Timor',
                'iso2'=>'TL',
                'iso3'=>'TLS',
                'calling_code'=>670,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Togo',
                'iso2'=>'TG',
                'iso3'=>'TGO',
                'calling_code'=>228,
                'currency_code'=>'XOF'
            ],        [
                'name'=>'Tonga',
                'iso2'=>'TO',
                'iso3'=>'TON',
                'calling_code'=>676,
                'currency_code'=>'TOP'
            ],        [
                'name'=>'Trinidad and Tobago',
                'iso2'=>'TT',
                'iso3'=>'TTO',
                'calling_code'=>1868,
                'currency_code'=>'TTD'
            ],        [
                'name'=>'Tunisia',
                'iso2'=>'TN',
                'iso3'=>'TUN',
                'calling_code'=>216,
                'currency_code'=>'TND'
            ],        [
                'name'=>'Turkmenistan',
                'iso2'=>'TM',
                'iso3'=>'TKM',
                'calling_code'=>993,
                'currency_code'=>'TMT'
            ],        [
                'name'=>'Turks and Caicos Islands',
                'iso2'=>'TC',
                'iso3'=>'TCA',
                'calling_code'=>1649,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Tuvalu',
                'iso2'=>'TV',
                'iso3'=>'TUV',
                'calling_code'=>688,
                'currency_code'=>'AUD'
            ],        [
                'name'=>'Uzbekistan',
                'iso2'=>'UZ',
                'iso3'=>'UZB',
                'calling_code'=>998,
                'currency_code'=>'UZS'
            ],        [
                'name'=>'Vanuatu',
                'iso2'=>'VU',
                'iso3'=>'VUT',
                'calling_code'=>678,
                'currency_code'=>'VUV'
            ],        [
                'name'=>'U.S. Virgin Islands',
                'iso2'=>'VI',
                'iso3'=>'VIR',
                'calling_code'=>1340,
                'currency_code'=>'USD'
            ],        [
                'name'=>'Yemen',
                'iso2'=>'YE',
                'iso3'=>'YEM',
                'calling_code'=>967,
                'currency_code'=>'YER'
            ],        [
                'name'=>'Zambia',
                'iso2'=>'ZM',
                'iso3'=>'ZMB',
                'calling_code'=>260,
                'currency_code'=>'ZMW'
            ],        [
                'name'=>'Zimbabwe',
                'iso2'=>'ZW',
                'iso3'=>'ZWE',
                'calling_code'=>263,
                'currency_code'=>'ZWL'
            ]
        ];
    }
}