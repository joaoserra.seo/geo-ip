<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpv4LocationsTable extends Migration
{
    public function up()
    {
        Schema::create('ipv4_locations', function (Blueprint $table) {
            $table->integer('block_start', false, true);
            $table->integer('block_end', false, true);

            $table->char('country_code', 2)->nullable();
            $table->string('mobile_country_code', 128)->nullable();
            $table->string('mobile_network_code', 128)->nullable();
            $table->string('operator_name', 128)->nullable();

            // Indexes
            // only block_end is commonly used currently, but the index contains both block keys
            // in order to allow changing the algorithm to use both blocks while maintaining it's indexation.
            $table->primary(['block_end', 'block_start']);
            $table->index(['country_code', 'operator_name'], 'idx_country_operators');
        });
    }

    public function down()
    {
        Schema::dropIfExists('ipv4_locations');
    }
}
