<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpv6LocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ipv6_locations', function (Blueprint $table) {
            // These are much larger than their IPV4 Counterparts, hence the decimal requirement.
            $table->decimal('block_start', 39,0)->unsigned();
            $table->decimal('block_end', 39,0)->unsigned();

            $table->char('country_code', 2)->nullable();
            $table->string('mobile_country_code', 128)->nullable();
            $table->string('mobile_network_code', 128)->nullable();
            $table->string('operator_name', 128)->nullable();

            // Indexes
            $table->primary(['block_end', 'block_start']);
            $table->index(['country_code', 'operator_name'], 'idx_country_operators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ipv6_locations');
    }
}
