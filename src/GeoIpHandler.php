<?php

namespace Oxy\GeoIP;

use IPTools\IP;
use Oxy\GeoIP\Exceptions\Ipv4TableEmptyException;
use Oxy\GeoIP\Exceptions\Ipv6TableEmptyException;
use Oxy\GeoIP\Queries\GeolocationQuery;
use Oxy\GeoIP\Queries\MobileOperatorsQuery;
use Oxy\GeoIP\Result\LocateResult;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GeoIpHandler
 *
 * Handles requests for GeoIP information from a given IP Address while
 * also providing easy access to several utilities for IP based information.
 *
 * @package App\Hotgold\Services
 * @author  Joao Serra <joao.serra@oxy.agency>
 */
class GeoIpHandler
{
    /**
     * @var string IP Address string (raw) that is currently set on the Class
     */
    protected $ipAddress;

    /**
     * Get an Eloquent Model for the Database Table representing the
     * current IP Address format.
     *
     * @return string The correct Eloquent Model for the current IP Version.
     */
    public function getTableForIpType()
    {
        $ipv4TableName = config('geoip.tables.ipv4', 'ipv4_locations');
        $ipv6TableName = config('geoip.tables.ipv6', 'ipv6_locations');

        $ipTools = new IP($this->ipAddress);
        $version = $ipTools->getVersion();
        return $version === 'IPv4' ? $ipv4TableName : $ipv6TableName;
    }

    /**
     * Return the GeoLocation data for the Given IP Address from the GeoIP Database.
     *
     * @param string $ipAddress
     * @param bool $testing
     *
     * @return LocateResult
     */
    public function locate($ipAddress = null, $testing = false)
    {
        $this->ipAddress = $ipAddress;

        if ( is_null($ipAddress) ) {
            // Resolve IP Address from PHP SuperGlobals
            $this->ipAddress = $this->resolveIpFromSuperglobals();
        }
        if ( $testing ) {
            // Random IPV4 Address for testing Purposes
            $this->ipAddress = $this->generateRandomIpAddress();
        }

        $ipTools = new IP($this->ipAddress);
        $ipAsLong = $ipTools->toLong();

        $this->checkIpTablesHaveRecords($ipTools);

        $results  = $this->queryGeolocationResults($ipAsLong);

        return new LocateResult($results, $ipTools->getVersion());
    }

    /**
     * Fetch all Mobile Operators from the IPV4 and IPV6 Tables and return them all
     * as a single Collection of unique string values along with their Country Codes.
     */
    public function getMobileOperators()
    {
        return MobileOperatorsQuery::get();
    }

    /**
     * Query the GeoLocation Database and Fetch the Results for the given IP Address
     *
     * @param string $ipAddressAsLong IP Address previously converted into a Long string
     *                                that will be matched against the GeoLocation blocks.
     *
     * @return Model
     */
    private function queryGeolocationResults( $ipAddressAsLong )
    {
        /** @var string $tableName */
        $tableName = $this->getTableForIpType();

        return GeolocationQuery::get($ipAddressAsLong, $tableName);
    }

    /**
     * Used to return a random IP Address which can be used in testing
     * to avoid having to manually set random IPs.
     *
     * @return string
     */
    private function generateRandomIpAddress()
    {
        return "" . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255);
    }

    /**
     * Attempt to resolve the Request IP Address from PHP Superglobals
     * if it cannot be decoded return null instead.
     *
     * @return string|null
     */
    private function resolveIpFromSuperglobals()
    {
        if ( getenv('HTTP_CLIENT_IP') ) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        }
        else if ( getenv('HTTP_X_FORWARDED_FOR') ) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        }
        else if ( getenv('HTTP_X_FORWARDED') ) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        }
        else if ( getenv('HTTP_FORWARDED_FOR') ) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        }
        else if ( getenv('HTTP_FORWARDED') ) {
            $ipaddress = getenv('HTTP_FORWARDED');
        }
        else if ( getenv('REMOTE_ADDR') ) {
            $ipaddress = getenv('REMOTE_ADDR');
        }
        else {
            $ipaddress = null;
        }

        return $ipaddress;
    }

    /**
     * Checks if the ipv4 and ipv6 tables contain any records, throwing exceptions if they do not.
     *
     * @param IP $ipTools
     */
    private function checkIpTablesHaveRecords($ipTools)
    {
        if($ipTools->getVersion() === 'IPv4') {
            $ipv4Check = \DB::table(config('geoip.tables.ipv4', 'ipv4_locations'))
                            ->exists();

            throw_unless($ipv4Check, new Ipv4TableEmptyException());
        }
        else {
            $ipv6Check = \DB::table(config('geoip.tables.ipv6', 'ipv6_locations'))
                            ->exists();

            throw_unless($ipv6Check, new Ipv6TableEmptyException());
        }
    }
}