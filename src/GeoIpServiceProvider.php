<?php

namespace Oxy\GeoIP;

use Illuminate\Support\ServiceProvider;

class GeoIpServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish Configuration
        $this->publishes([
            __DIR__ . '/Config/geoip.php' => config_path('geoip.php')
        ], 'config');

        $this->publishes([
            __DIR__ . '/Migrations/' => database_path('migrations')
        ], 'migrations');

        // Load Database Migrations
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

        // Load any Artisan Commands the Package Exposes
        if($this->app->runningInConsole()) {
            $this->commands([
                //
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Merge the package config with the published version allowing user to skip defaults
        $this->mergeConfigFrom(
            __DIR__ . '/Config/geoip.php', 'geoip'
        );

        $this->app->bind('oxy.geoip', function($app) {
            return new GeoIpHandler();
        });
    }

    /**
     * Returns list of Services provided by the Package.
     *
     * @return array
     */
    public function provides()
    {
        return ['oxy.geoip'];
    }
}
