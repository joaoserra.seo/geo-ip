<?php

return [

    'tables' => [
        'ipv4'      => 'ipv4_locations',
        'ipv6'      => 'ipv6_locations',
        'countries' => 'countries'
    ],

    'columns' => [
        'block_start'         => 'block_start',
        'block_end'           => 'block_end',
        'country_code'        => 'country_code',
        'mobile_country_code' => 'mobile_country_code',
        'mobile_network_code' => 'mobile_network_code',
        'operator_name'       => 'operator_name',
    ]

];