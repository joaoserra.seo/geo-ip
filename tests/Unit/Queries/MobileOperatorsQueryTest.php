<?php

namespace Oxy\GeoIP\Tests\Unit\Queries;

use Oxy\GeoIP\Tests\TestCase;
use Illuminate\Support\Collection;
use Oxy\GeoIP\Queries\MobileOperatorsQuery;

class MobileOperatorsQueryTest extends TestCase
{
    /** @test */
    public function it_should_return_all_mobile_operators_data()
    {
        $this->createMultipleLocations();

        $result = MobileOperatorsQuery::get();
        $this->assertNotNull($result);
        $this->assertInstanceOf(Collection::class, $result, "The result-set is not a valid Collection instance");
        $array = $result->transform(function($item) {
            return (array)$item;
        });
        $this->assertArraySubset([
            [
                'country_code'  => 'PK',
                'operator_name' => 'Ufone'
            ],
            [
                'country_code'  => 'PT',
                'operator_name' => 'MEO'
            ]
        ], $array);
    }
}