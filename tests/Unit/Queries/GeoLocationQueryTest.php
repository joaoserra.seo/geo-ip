<?php

namespace Oxy\GeoIP\Tests\Unit\Queries;

use Oxy\GeoIP\Tests\TestCase;
use Oxy\GeoIP\Queries\GeolocationQuery;

class GeoLocationQueryTest extends TestCase
{
    /** @test */
    public function it_should_return_geolocation_data_for_the_given_ip_address()
    {
        $this->createLocationIpv4PakistanMobile();

        $result = GeolocationQuery::get("657542654", 'ipv4_locations');
        $this->assertNotNull($result);
        $this->assertInstanceOf(\stdClass::class, $result, "The result that was returned was not a valid class instance.");
        $array = (array)$result;
        $this->assertArraySubset([
            'block_start'         => 657542144,
            'block_end'           => 657542655,
            'country_code'        => 'PK',
            'mobile_country_code' => '410',
            'mobile_network_code' => '03',
            'operator_name'       => 'Ufone'
        ], $array);
    }
}