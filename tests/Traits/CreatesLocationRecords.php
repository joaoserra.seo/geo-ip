<?php

namespace Oxy\GeoIP\Tests\Traits;

trait CreatesLocationRecords
{
    public function createLocationIpv4PakistanMobile()
    {
        \DB::table('ipv4_locations')->insert([
            'block_start'         => 657542144,
            'block_end'           => 657542655,
            'country_code'        => 'PK',
            'mobile_country_code' => "410",
            'mobile_network_code' => "03",
            'operator_name'       => "Ufone"
        ]);
    }

    public function createMultipleLocations()
    {
        \DB::table('ipv4_locations')->insert([
            'block_start'         => 657542144,
            'block_end'           => 657542655,
            'country_code'        => 'PK',
            'mobile_country_code' => "410",
            'mobile_network_code' => "03",
            'operator_name'       => "Ufone"
        ]);

        \DB::table('ipv4_locations')->insert([
            'block_start'         => 657542656,
            'block_end'           => 687542655,
            'country_code'        => 'PT',
            'mobile_country_code' => "400",
            'mobile_network_code' => "07",
            'operator_name'       => "MEO"
        ]);
    }
}