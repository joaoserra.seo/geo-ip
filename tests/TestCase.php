<?php

namespace Oxy\GeoIP\Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use Oxy\GeoIP\Tests\Traits\CreatesLocationRecords;

class TestCase extends BaseTestCase
{
    use CreatesLocationRecords;

    public function setUp()
    {
        parent::setUp();

        $this->artisan('migrate:fresh', ['--database' => 'geoiptesting']);
    }

    protected function getPackageProviders($app)
    {
        return [
            \Oxy\GeoIP\GeoIpServiceProvider::class
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'geoiptesting');
        $app['config']->set('database.connections.geoiptesting', [
            'driver' => 'mysql',
            'host' => 'mysql',
            'port' => '3306',
            'database' => 'geoiptesting',
            'username' => 'root',
            'password' => 'root',
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ]);
    }
}